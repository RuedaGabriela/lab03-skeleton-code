package course.labs.activitylab;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

public class ActivityOne extends Activity {

		// string for logcat documentation
		private final static String TAG = "Lab-ActivityOne";
		private int countCreate = 0;
		private int countStart = 0;
		private int countResume = 0;
		private int countRestart= 0;
		private int countPause = 0;
		private int countStop = 0;
		private int countDestroy = 0;
		// lifecycle counts

		@Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.activity_one);
			countCreate++;
			//Log cat print out
			Log.i(TAG, "onCreate called, count: "+ countCreate);
			TextView tv = (TextView) findViewById(R.id.create);
			tv.setText(String.valueOf(countCreate));

		}

		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			// Inflate the menu; this adds items to the action bar if it is present.
			getMenuInflater().inflate(R.menu.activity_one, menu);
			return true;
		}

		@Override
		protected void onStart(){
			super.onStart();
			countStart++;
			//Log cat print out
			Log.i(TAG, "onStart called, count:" + countStart);
			TextView tv = (TextView) findViewById(R.id.start);
			tv.setText(String.valueOf(countStart));
		}

		@Override
		protected void onResume(){
		super.onResume();
		countResume++;
		//Log cat print out
		Log.i(TAG, "onResume called, count:" + countResume);
			TextView tv = (TextView) findViewById(R.id.resume);
			tv.setText(String.valueOf(countResume));
		}

		@Override
		protected void onPause(){
		super.onPause();
		countPause++;
		//Log cat print out
		Log.i(TAG, "onResume called, count:" + countPause);
			TextView tv = (TextView) findViewById(R.id.pause);
			tv.setText(String.valueOf(countPause));
	}

		@Override
		protected void onStop(){
		super.onStop();
		countStop++;
		//Log cat print out
		Log.i(TAG, "onResume called, count:" + countStop);
			TextView tv = (TextView) findViewById(R.id.stop);
			tv.setText(String.valueOf(countStop));
	}

		@Override
		protected void onDestroy(){
		super.onDestroy();
		countDestroy++;
		//Log cat print out
		Log.i(TAG, "onResume called, count:" + countDestroy);
			TextView tv = (TextView) findViewById(R.id.destroy);
			tv.setText(String.valueOf(countDestroy));
	}

		@Override
		protected void onRestart(){
		super.onRestart();
		countRestart++;
		//Log cat print out
		Log.i(TAG, "onResume called, count:" + countRestart);
			TextView tv = (TextView) findViewById(R.id.restart);
			tv.setText(String.valueOf(countRestart));
	}


		@Override
		public void onSaveInstanceState(Bundle savedInstanceState){
			//TODO:  save state information with a collection of key-value pairs & save all  count variables
		}


		public void launchActivityTwo(View view) {
			startActivity(new Intent(this, ActivityTwo.class));
		}
		

}
