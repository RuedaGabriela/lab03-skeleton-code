package course.labs.activitylab;

import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.View;

public class ActivityTwo extends Activity {

	// string for logcat documentation
	private final static String TAG = "Lab-ActivityTwo";

	private int countCreate = 0;
	private int countStart = 0;
	private int countResume = 0;
	private int countRestart = 0;
	private int countPause = 0;
	private int countStop = 0;
	private int countDestroy = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_two);
		
		//Log cat print out
		Log.i(TAG, "onCreate called, count :"+ countCreate);
		
	}

	@Override
	protected void onStart(){
		super.onStart();
		countStart++;
		//Log cat print out
		Log.i(TAG, "onStart called, count:" + countStart);
	}

	@Override
	protected void onResume(){
		super.onResume();
		countResume++;
		//Log cat print out
		Log.i(TAG, "onResume called, count:" + countResume);
	}

	@Override
	protected void onPause(){
		super.onPause();
		countPause++;
		//Log cat print out
		Log.i(TAG, "onResume called, count:" + countPause);
	}

	@Override
	protected void onStop(){
		super.onStop();
		countStop++;
		//Log cat print out
		Log.i(TAG, "onResume called, count:" + countStop);
	}

	@Override
	protected void onDestroy(){
		super.onDestroy();
		countDestroy++;
		//Log cat print out
		Log.i(TAG, "onResume called, count:" + countDestroy);
	}

	@Override
	protected void onRestart(){
		super.onRestart();
		countRestart++;
		//Log cat print out
		Log.i(TAG, "onResume called, count:" + countRestart);
	}

}
